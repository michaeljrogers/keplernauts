//Michael Rogers
//Keplernauts
//2015

//imports
import controlP5.*;
import ddf.minim.*;


//global variables
ArrayList<String> starlist = new ArrayList(); // A list that will contain all of the star names from list
ArrayList<Star> stars = new ArrayList(); //A list that will contain all stars generated from Kepler data
int background_stars= 2000; //background stars
float xStars[] = new float[background_stars]; //background stars' x coord
float yStars[] = new float[background_stars]; //background stars' y coord
Table table; //table that will hold csv data
PFont font; //The font that will be used on just about everything in this sketch
PImage earth; //Earth image for home base background
PImage logo;//Logo image
int selectedIndex=0;
boolean drawEarth= true;//Helper variable for turning the Earth image on and off
boolean drawSolar=false;//Helper variable displaying solar View and turning it off.
boolean paths = true;//Whether or not to view orbit rings in Solar View.
int time=0; //Controls the speed at which planets revolve

String bigStar;
float minRad=500;
float maxRad=0;
ArrayList<String> playList;

//cP5 variables
ControlP5 cp5; //creates controller
controlP5.Button travelButton; //button in menu to open solar View
controlP5.Button homeButton; //button in menu to return to homeBase view
controlP5.Button menuButton; //button in menu to open menu view
ListBox starDropdown; //list of stars that appears in menu. Select a star to open solar view
ControlFont cFont;//Font to be used for control items
ControlFont cFontSmall;//Font to be used for control items
Textarea destination;//displays selected star from dropdown list
Textarea menuInfo; //info box that appears with info of menu that is currently being viewed.
Textarea homeInfo; //info box that appears with info on the home menu.
Textarea starInfo;// info box that appears with info on the solar view
Toggle orbitRingToggle; //toggles rings on or off solar view
Slider timeSlider; //adjusts rate of revolution in planet view
Button previousStar;//goes to previous star in view
Button nextStar;//goes to next star in solar view
Textarea starName;//displays star Name on solar view page
Button reset;//displays reset button on solar view that sets planet positions to 0

AudioPlayer effect;
Minim minim;
AudioPlayer music;
int songIndex=0;
void setup() {
  fullScreen();
  smooth();
  sound();
  cp5 = new ControlP5(this);//finishes controller setup.
  font = createFont("LCD_Solid.ttf", 32); //imports font
  cFont= new ControlFont(font, 20); //allows for use with cp5
  cFontSmall= new ControlFont(font, 15); //allows for use with cp5

  earth=loadImage("earth.png");
  logo=loadImage("logo.png");
  for (int x = 0; x<background_stars; x++) { //Generates random background star locations
    xStars[x]=random(width);
    yStars[x]= random(height);
  }
  csvReader();//initializes the csv file
  controllerSetup();//create cp5 modules
  println("After setup, starlist has "+starlist.size()+" stars "+ " from the inital "+table.getRowCount()+" rows");
};

void draw() {

  background(0, 0, 0);
  for (int x = 0; x<background_stars; x++) { //places background stars
    noStroke();
    fill(255, 255, 255, random(30, 150));   //having random for opacity makes the stars flicker on each loop of draw
    ellipse(xStars[x], yStars[x], 3, 3);    //places said stars
  }
  menuChange(); //Helper method to help change the menus
  destination.setText("Destination:\n"+starlist.get(selectedIndex));
  if (drawEarth) { //Decides whether or not to draw earth (decided in menuChange())
    image(earth, -100, 0, 800, 800);
    image(logo, width/2-140, -20,  562*1.5, 212*1.5);
  }
  if (drawSolar) {
    Star starobj=stars.get(selectedIndex);
    solarView(starobj);
  }

  if (!music.isPlaying()) {
    songIndex++;
    if (songIndex>playList.size()-1) {
      songIndex=0;
    }
    music = minim.loadFile(playList.get(songIndex));
    music.play();
  }
};


void solarView(Star starobj) {// will draw the view of selected solar system
  String starname=starobj.name;
  float starRadius=starobj.rad;
  float starDistance=starobj.dist;
  

  ArrayList<Planet> planets=starobj.getPlanets();
  int planetCount=planets.size();
  starobj.render();


  String starInfoText="Star Information\n\n"
    +"Star Name:                 "+starname+"\n"
    +"Distance from Earth:       "+starDistance+" parsecs\n"
    +"Star Radius:               "+starRadius+" x 10^3 km\n"
    +"Planet Count:              "+planetCount+"\n\n\n"
    +"Planets (Left to Right):\n";
  for (int i=0; i< planets.size(); i++) { 

    Planet newPlanet = planets.get(i);

    newPlanet.render(starRadius, paths, time);
    String planetName =newPlanet.letter;
    starInfoText = starInfoText + planetName; 
    if (i+1!=planets.size()) {
      starInfoText = starInfoText+", ";
    }
  }
  if (reset.getBooleanValue()) {
    effect=minim.loadFile("travel1.mp3");
    effect.play();
    for (int i=0; i< planets.size(); i++) { 
      Planet newPlanet = planets.get(i);
      newPlanet.refresh();
    }
    reset.setOff();
    time=0;
    timeSlider.setValue(0);
    timeSlider.update();
  }

  starInfo.setText(starInfoText);
  starName.setText(starname);
}











void menuChange() { //This method will check the conditions to see when the menus need to change.
  if (menuButton.getBooleanValue()) {
    effect=minim.loadFile("menu.mp3");
    effect.play();
    drawEarth=false; 
    drawSolar=false;
    menuButton.setOff();
    menuButton.hide();
    homeInfo.hide();


    destination.show();
    travelButton.show();
    menuInfo.show();
    homeButton.show();
    starDropdown.show();

    orbitRingToggle.hide();
    timeSlider.hide();
    previousStar.hide();
    nextStar.hide();
    starInfo.hide();
    starName.hide();
    reset.hide();
  }
  if (homeButton.getBooleanValue()) {
    effect=minim.loadFile("home.mp3");
    effect.play();
    drawEarth=true;
    drawSolar=false;
    homeButton.setOff();
    menuButton.show();
    homeInfo.show();


    destination.hide();
    travelButton.hide();
    menuInfo.hide();
    homeButton.hide();
    starDropdown.hide();

    orbitRingToggle.hide();
    timeSlider.hide();
    previousStar.hide();
    nextStar.hide();
    starInfo.hide();
    starName.hide();
    reset.hide();
  }
  if (nextStar.getBooleanValue()) {
    if (selectedIndex!=stars.size()-1) {
      selectedIndex=selectedIndex+1;
      
    }
    nextStar.setOff();
  }
  if (previousStar.getBooleanValue()) {
    if (selectedIndex!=0) {
      selectedIndex=selectedIndex-1;
      
    }
    previousStar.setOff();
  }



  if (travelButton.getBooleanValue()) {
    effect=minim.loadFile("c.mp3");
    effect.play();
    drawEarth=false; 
    drawSolar=true;
    travelButton.setOff();
    menuButton.show();
    homeInfo.hide();

    destination.hide();
    travelButton.hide();
    menuInfo.hide();
    homeButton.hide();
    starDropdown.hide();

    orbitRingToggle.show();
    timeSlider.show();
    previousStar.show();
    nextStar.show();
    starInfo.show();
    starName.show();
    reset.show();
  }
}





void controllerSetup() {//will draw the homebase screen. Consists of picture of Earth on stary background

  //Start Screen Controls  
  menuButton=cp5.addButton("Main Menu") //upper left, open menu()
    .setPosition(width-200, 0)
    .setSize(200, 50)
    .setColorBackground(color(255, 100))
    .setSwitch(true)
    ;
  cp5.getController("Main Menu") //sets font for menu button
    .getCaptionLabel()
    .setFont(cFont)
    ;
  homeInfo = cp5.addTextarea("Home Info") //Text box with information about homebase page
    .setPosition(width-600, height-175)
    .setSize(600, 175)
    .setFont(cFontSmall)
    .setLabel("Menu Info")
    .setColorBackground(color(255, 100))
    .setText("Welcome to Keplernauts!\n"
    +"Keplernauts is a program that allows you to view a selection of Exoplanetary Solar Systems from  the Kepler archives. "
    +"To begin, open the MAIN MENU in the upper-right corner. From there, you will see a catalog of Stars. \n\n"
    +"Keplernauts was built using Processing and is only in its second iteration.\n\n"
    +"Created by Michael Rogers"
    );

  //Main Menu Controls
  menuInfo = cp5.addTextarea("Menu Info")
    .setPosition(0, height-200)
    .setSize(450, 200)
    .setFont(cFontSmall)
    .setLabel("Menu Info")
    .setColorBackground(color(255, 100))
    .hide()
    .setText("All right, let's go over the main menu functions.\n"
    +"From this menu, you can select any of the Catalogued Stars in the list"
    +" and open it in Solar View. To do this, simply select"
    +" the star you would like to view, and click TRAVEL. Your active destination can be viewed below the TRAVEL button."
    +" Explore around!"+ "\n\n"
    +"If you wish to return to the HOME MENU, simply click the RETURN TO HOME MENU button "
    +"in the upper left corner of the screen. "+ "\n\n"
    );
  homeButton=cp5.addButton("Return to Home Menu") //upper left, open homeBase()
    .setPosition(0, 0)
    .setSize(250, 50)
    .setColorBackground(color(255, 100))
    .setSwitch(true)
    .hide()
    ;
  cp5.getController("Return to Home Menu") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;

  starDropdown= cp5.addListBox("Catalogued Stars")//right side of screen will contain list of all stars.
    .setPosition(width-250, 0)
    .setSize(250, height)
    .setItemHeight(30)
    .setBarHeight(40)
    .setId(1)
    .setColorBackground(color(255, 100))
    .hide()
    ;
  cp5.getController("Catalogued Stars") //sets font for Caption label
    .getCaptionLabel()
    .setFont(cFont)
    ;

  travelButton=cp5.addButton("Travel") //upper right, open solarView()
    .setPosition(width-520, 0)
    .setSize(250, 50)
    .setColorBackground(color(255, 100))
    .setSwitch(true)
    .hide()
    ;
  cp5.getController("Travel") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;
  destination = cp5.addTextarea("Destination")
    .setPosition(width-520, 60)
    .setSize(250, 50)
    .setFont(cFontSmall)
    .setLabel("Menu Info")
    .setColorBackground(color(255, 100))
    .hide()
    .setText("Destination:\n"+starlist.get(selectedIndex));
  ;

  int i=0;
  for (Star star : stars) { //populates listbox
    String starName=starlist.get(i);
    starDropdown.addItem(starName, star); 
    i+=1;
  };

  //Solar View Controls
  orbitRingToggle=cp5.addToggle("paths")
    .setPosition(width -200, 100)
    .setSize(200, 50)
    .setValue(true)
    .setLabel("Orbit Paths")
    .setMode(ControlP5.SWITCH)
    .setColorBackground(color(255, 100))
    .hide()
    ;
  cp5.getController("paths") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;

  timeSlider=cp5.addSlider("time")
    .setPosition(100, height-50)
    .setWidth(400)
    .setRange(0, 8)
    .setValue(time)
    .setLabel("Time Flow")
    .setNumberOfTickMarks(5)
    .setId(3)
    .hide();

  cp5.getController("time") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;
  previousStar=cp5.addButton("Previous\nStar")
    .setPosition(width/2, height-80)
    .setSize(100, 80)
    .setColorBackground(color(255, 100))
    .setSwitch(true)
    .hide();

  cp5.getController("Previous\nStar") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;
  nextStar=cp5.addButton("Next\nStar")
    .setPosition(width/2 +100, height-80)
    .setSize(100, 80)
    .setColorBackground(color(160, 100))
    .setSwitch(true)
    .hide();

  cp5.getController("Next\nStar") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;
  starInfo = cp5.addTextarea("Star Info") //Text box with information about the star being viewed
    .setPosition(width-475, height/2+200)
    .setSize(475, 200)
    .setFont(cFontSmall)
    .setLabel("Menu Info")
    .setColorBackground(color(255, 100))
    .hide()
    ;
  starName = cp5.addTextarea("Star Name") //Text box with information about the star being viewed
    .setPosition(0, 0)
    .setSize(300, 50)
    .setFont(cFont)
    .setLabel("Star Name")
    .setColorBackground(color(255, 100))
    .hide()
    ;
  reset = cp5.addButton("Reset")
    .setPosition(width/2-220, height-120)
    .setSize(150, 45)
    .setColorBackground(color(160, 100))
    .setSwitch(true)
    .hide();

  cp5.getController("Reset") //sets font for homebase button
    .getCaptionLabel()
    .setFont(cFont)
    ;
}








void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.

  if (theEvent.isController()) {
    int eventId=theEvent.getId();//Tells us which controller is sending the signal

    if (eventId==1) {
      selectedIndex=int(theEvent.getController().getValue());
      effect=minim.loadFile("travel1.mp3");
      effect.play();
      println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
    }
  }
}

void csvReader() {
  table = loadTable("kepler.csv", "header"); //Loads our csv data
  for (TableRow row : table.rows()) { 
    String pl_hostname = row.getString("pl_hostname"); //star name
    String pl_letter = row.getString("pl_letter"); //planet letter
    float pl_rade = row.getFloat("pl_rade"); //planet radius (earth)
    float st_rad = row.getFloat("st_rad"); //star radius
    float st_dist = row.getFloat("st_dist"); //star distances in parsecs

    if (starlist.contains(pl_hostname)) { //if we have already handled the star, we just need to modify its planet data
      int end=stars.size()-1; //gets last indext of stars arrayList
      Star starobj=stars.get(end); //gets star at last index
      stars.remove(end); //removes last star
      starobj.addPlanet(pl_letter, pl_rade, st_rad); //adds new planet data to star
      stars.add(starobj); //adds updated star back to list
    } else {
      Star starobj =  new Star(pl_hostname, pl_letter, pl_rade, st_rad, st_dist);//creates new star
      stars.add(starobj); //adds star to list
      starlist.add(pl_hostname);//adds name to starlist so that we know that it is a star that has been processed.
    }
  }
};

void sound() {
  playList= new ArrayList();

  playList.add("theme.mp3");
  playList.add("ambience.mp3");
  playList.add("ambience.mp3");


  minim=new Minim(this);//audio context
  music = minim.loadFile(playList.get(songIndex));
  music.play();
  effect = minim.loadFile("c.mp3");
};

void stop()
{
  music.close();
  effect.close();
  minim.stop();
  super.stop();
} 