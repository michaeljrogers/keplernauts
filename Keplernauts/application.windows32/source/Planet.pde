class Planet {
  String letter; //planet letter, exception of sol, planet name is star name plus planet letter
  float radius; //radius proportiante to earth
  float distance;
  int position;
  float xPos;
  float yPos;
  int r;
  int g;
  int b;
  float inc=-1; 
  float angle;
  
  
  float earthRadius = 6;//number of pixel representative of 1 earth radius
  Planet(String letter, float radius, int position) {
    this.letter= letter;
    this.radius=radius*earthRadius;
    this.position=position;
    r=(int)random(50,250);
    g=(int)random(0,250);
    b=(int)random(100,250);
    angle=0;
    
  };
  
  public void refresh() {
    angle=0;
  };
  
  public void render(float starRadius, boolean orbitRing, int timeFlow) {
    distance=starRadius/2+100*position;
    inc=2*PI/distance;
    angle += inc*timeFlow/2;
    if(angle== 2*PI){
       angle=0; 
    }
    xPos = (int)(cos(angle)*distance); //This line and the next are the main things I really needed to do research on. However, this equation was so widely proliferated
    yPos = (int)(sin(angle)*distance); 
    fill(r,g,b);
    stroke(255);
    ellipse((width/2-starRadius/2-200)+xPos, height/2+yPos, radius, radius);
    if (orbitRing){
    noFill();
    stroke(r,g,b);
    ellipse(width/2-starRadius/2-200, height/2,starRadius+200*position,starRadius+200*position);
    }
  };
};