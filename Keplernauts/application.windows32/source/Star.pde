class Star {
  String name;//star name
  ArrayList<Planet> planets = new ArrayList(); //list of planets that star has
  float rad; //star radius
  float dist; //star dist from sol
  float sunRadius = 696.3;
  Star(String pl_hostname, String pl_letter, float pl_rade, float st_rad, float st_dist) {
    name=pl_hostname;
    rad = st_rad*sunRadius;
    addPlanet(pl_letter, pl_rade, rad); //creates a new planet and adds to planets array
    dist = st_dist;
  };
  ArrayList getPlanets(){
    return planets;
  };
  public void init() {
  };
  public void update() {
  };
  public void render() {
    if ( rad< 500){
    fill(250,100,0);
    }
    else if(rad< 800){
      fill(200,240,0);
    }
    else{
      fill(250,0, 0);
    }
    noStroke();
    ellipse(width/2-rad/2-200, height/2 ,rad, rad);
  };
  public void addPlanet(String letter, float radius, float starRadius) {
    int position=planets.size()+1;
    Planet planetObj = new Planet(letter, radius, position); //creates planet
    planets.add(planetObj); //adds to array
  };
}